# bastille_caddy
Bastille template to bootstrap Caddy. An easy way to create a Caddy server in a BSD jail.

## Bootstrap the template

```shell
bastille bootstrap https://gitlab.com/russelltadams/bastille_caddy
```

## Use the template

Create a base jail for Caddy if you haven't already. Here's a simple example. In this example `mycaddyjail` is the name of the jail, the "target" of the bastille template.  

Make a base jail.   
```shell
bastille create mycaddyjail 12.2-RELEASE 192.168.100.42 em0
```

Apply template to the jail. This will install Caddy and your optional config.   
```shell
bastille template mycaddyjail russelltadams/bastille_caddy  

```

See [https://bastillebsd.org]() for more details on using Bastille.  

## Optional:
The following are optional customizations to this template and basic
instructions on how to do so. Either fork or copy and publish the repo to save and upload your customization for easy bootstrapping of your Caddy server jail.

### custom config
Use a custom `/bastille_caddy/usr/local/etc/caddy/Caddyfile` in the template:

1. edit `/bastille_caddy/usr/local/etc/caddy/Caddyfile`
2. commit; push; bootstrap; apply template

### add your application files to Caddy:
Add your own website/application/files:

1. remove  `/bastille_caddy/usr/local/www/caddy/index.html`
2. add your own files to `bastille_caddy/usr/local/www/caddy/`
3. commit; push; bootstrap; apply template
